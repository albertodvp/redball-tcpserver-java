import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by alberto on 1/11/17.
 */
public class ServerBall {
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;

    private DataOutputStream outputStream = null;
    private DataInputStream inputStream = null;

    public ServerBall(){

    }

    public boolean isClientSet(){
       if(clientSocket == null){
           return false;
       } else {
           return true;
       }
    }

    public void waitClient() throws Exception {
        serverSocket = new ServerSocket(6789);
        System.out.println("Aspetto il client");
        do {
            clientSocket = serverSocket.accept();
            outputStream = new DataOutputStream(clientSocket.getOutputStream());
            inputStream = new DataInputStream(clientSocket.getInputStream());
        } while (clientSocket == null);
        System.out.println("Connessione stabilita con: " + serverSocket);
    }

    public void writeBallStartingPosition(int x, int y) throws Exception{
       if(outputStream == null){
           System.out.println("Output stream is null");
           return;
       }
       System.out.println("Scrivo le posizioni al client");
       outputStream.write(x);
       outputStream.write(y);
    }
    public void waitForPosition() throws Exception{
        int x = inputStream.read();
        int y = inputStream.read();
        System.out.println("x : " + x + " -- y : " +y);
    }

}
