/**
 * Created by alberto on 1/11/17.
 */
public class Main {
    public static void main(String[] args){
        ServerBall serverBall = new ServerBall();
        try {
            serverBall.waitClient();
            if(serverBall.isClientSet()){
                serverBall.waitForPosition();
                serverBall.writeBallStartingPosition(10, 30);
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
