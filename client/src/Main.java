/**
 * Created by alberto on 1/11/17.
 */
public class Main {
    public static void main(String[] args){
        ClientBall clientBall = new ClientBall();
        try {
            clientBall.connect();
            clientBall.writeBallStartingPosition(10,20);
            clientBall.waitForPosition();
        } catch (Exception exc) {
           exc.printStackTrace();
        }

    }
}
