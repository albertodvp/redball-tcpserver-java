import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by alberto on 1/11/17.
 */
public class ClientBall {

    private Socket serverSocket = null;
    private DataOutputStream outputStream = null;
    private DataInputStream inputStream = null;

    public ClientBall(){

    }

    public void connect() throws Exception {
        serverSocket = new Socket("localhost",6789);
        outputStream = new DataOutputStream(serverSocket.getOutputStream());
        inputStream = new DataInputStream(serverSocket.getInputStream());
        System.out.println("Connessione stabilita con: " + serverSocket);
    }

    public void writeBallStartingPosition(int x, int y) throws Exception{
       if(outputStream == null){
           System.out.println("Output stream is null");
           return;
       }
       System.out.println("Scrivo le posizioni al server");
       outputStream.write(x);
       outputStream.write(y);
    }
    public void waitForPosition() throws Exception{
        int x = inputStream.read();
        int y = inputStream.read();
        System.out.print("x : " + x + " -- y : " +y);
    }

}
